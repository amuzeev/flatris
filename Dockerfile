FROM node

RUN mkdir /app
WORKDIR /app

COPY package.json /app
RUN yarn install

COPY . /app

# If no  WORKDIR then:
# RUN cd /app && yarn install

# RUN yarn test

RUN yarn build

# RUN для сборки
# CMD для запуска

# EXPOSE вытащить наружу порт
EXPOSE 3000
CMD yarn start 

